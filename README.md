# step5




МЕТОДЫ:


  ### ГЕНЕРИРОВАНИЕ СПИСКА рейсов и запись в файл ( если списка еще нету) :     FlightListCreator.generate();

### получение списка  рейсов из файла:FlightListCreator.getData()(до инициализации дао потому что ее каждый раз при запуске нужно инициализировать уже готовым списком)

1.Онлайн-табло:flightsController.showAllFlightsNext24Hours(List <Flight> flights ) - берет список рейсов  из ДАО И и показывает только рейсы на ближайшие 24 часа

2.Посмотреть информацию о рейсе: flightsController.findFlight(LocalDate date, String destination, int passengersCount) - принимает пункт назначения дату и кол во пассажиров и находит в базе рейсов нужный рейс
   (для использования в пункте 5 где нужно найти  и показать рейс  с нужными показателями передтем как забронировать )

3. flightsController.findFlightByID(int flightId) - принимает айди рейса( поле класса Flight)  и находит рейс с таким айди

4.Добавление бронирования:bookingController.addNewBooking(List<User> passengers ,Flight flight)- принимает список пассажиров который приходит из консоли и рейс(можнно получить методом flightController.findFlightByID 
 используя айди который пользователь ввел)

5Удаление бронирования:bookingController.deleteBooking(int bookingId) - принимает айди бронирования и удаляет его 

6 Посмотреть бронирования :bookingController.displayAllBookings(User passenger)-принимает пользователя и находит в списке бронирований все где он является пассажиром
  Выводит их на экран

ЕСТЬ ДОПОЛНИТЕЛЬНЫЕ МЕТОДЫ НАПРИМЕР В КЛАССЕ User: User.findUserInList(String name,String surname,List <User> userList)- можно найти юзера по имени и фамилии в списке юзеров который создается из всех юзеров которые есть в бронированиях

В КЛАССЕ Booking:LBooking.getBookings()- позволяет получить список бронирований из файла чтобы инициализировать им дао бронирований