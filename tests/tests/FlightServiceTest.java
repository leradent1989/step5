package tests;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import step5.controller.FlightsController;
import step5.dao.FlightsDao;
import step5.service.FlightsService;
import step5.domain.Flight;
import java.time.LocalTime;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FlightServiceTest{
    private FlightsService module;
    private ByteArrayOutputStream output = new ByteArrayOutputStream();
    List <Flight> flights = new ArrayList<>(List.of(new Flight(LocalDate.of(2021,10,13), LocalTime.of(16,40),"PARIS",5,1111),
            new Flight(LocalDate.of(2023,01,(LocalDate.now().getDayOfMonth())), LocalTime.of((LocalTime.now().getHour() +1),50),"LONDON",5,1211),
            new Flight(LocalDate.of(2021,10,13), LocalTime.of(18,30),"CHICAGO",6,1311),
            new Flight(LocalDate.of(2021,10,14), LocalTime.of(19,40),"WASHINGTON",10,1411)
    ) );

    @BeforeEach
    public void setUp() {

        FlightsDao flightsDao = new FlightsDao(flights);
        module = new FlightsService(flightsDao);
        FlightsController flightsController = new FlightsController(module);
        flights.add(new Flight(LocalDate.of(2023,2,10), LocalTime.of(19,20),"WASHINGTON",10,1711));

    }

    @Test
    void findAll() {
        List <Flight> actual =module.findAll();
        List <Flight> expected = flights;
        assertEquals(expected, actual);
    }

    @Test
    void showAllFlightsNext24Hours() {
        PrintStream old = System.out;
        String str ="Flight [N 1211; date:2023-01-" + LocalDate.now().getDayOfMonth()+ "; time:"+ LocalTime.of((LocalTime.now().getHour() +1),50)  +"; destination: LONDON; freeSeats:5 passengers: [] ]";
        System.setOut(new PrintStream(output));
        module.showAllFlightsNext24Hours(flights);
        assertEquals(output.toString().replaceAll("\n", ""),
                str
                , "Successfully brings text");
        System.setOut(old);

    }
    @Test
    void findFlightById() {
        Flight actual =  module.findFlightById(1211);
        Flight expected = flights.get(1);
        assertEquals(expected, actual);
    }

    @Test

    void findFlight() {
        LocalDate date =LocalDate.of(2023,1,(LocalDate.now().getDayOfMonth()));
        String destination ="London";
        int passengers = 3;
        Flight actual = module.findFlight(date,destination,passengers);
        Flight expected = flights.get(1);

        assertEquals(expected, actual);
    }

    @Test
    void addFlight() {
        Flight addedFlight =  new Flight(LocalDate.of(2023,2,10), LocalTime.of(19,20),"WASHINGTON",10,1611);
        module.addFlight(addedFlight);
        Flight actual = module.findFlightById(addedFlight.getId());
        Flight expected = addedFlight;

        assertEquals(expected, actual);
    }


}








