package tests;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import step5.controller.FlightsController;
import step5.controller.BookingController;
import step5.dao.BookingDao;
import step5.domain.Booking;
import step5.domain.User;
import step5.service.BookingService;
import step5.service.FlightsService;
import step5.domain.Flight;
import java.time.LocalTime;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class BookingDaoTest{
    private BookingDao module;
    private ByteArrayOutputStream output = new ByteArrayOutputStream();
    List <Flight> flights = new ArrayList<>(List.of(new Flight(LocalDate.of(2021,10,13), LocalTime.of(16,40),"Paris",5,1111),
            new Flight(LocalDate.of(2023,01,19), LocalTime.of(9,50),"London",5,1211),
            new Flight(LocalDate.of(2021,10,13), LocalTime.of(18,30),"New York",6,1311),
            new Flight(LocalDate.of(2021,10,14), LocalTime.of(19,40),"Washington",10,1411)
    ) );
    List <Booking> bookings1 = new ArrayList<>();
    List <Booking> bookings2 = new ArrayList<>();
    List <Booking> bookingList = new ArrayList<>();
    List <User> users = new ArrayList<>(List.of(new User("Valeriya","Vitvytska",bookings1),
            new User("Roman","Litvinenko",bookings2)));
    Booking booking1 = new Booking(users,users.size(),flights.get(0));
    Booking booking2 = new Booking(users,users.size(),flights.get(1));
    Booking booking3 = new Booking(users,users.size(),flights.get(2));
    Booking booking4 = new Booking(users,users.size(),flights.get(3));





    @BeforeEach
    public void setUp() {

        module = new BookingDao(bookingList);

        flights.add(new Flight(LocalDate.of(2023,2,10), LocalTime.of(19,20),"Washington",10,1711));

        bookings1.add(booking1);
        bookings1.add(booking3);
        bookings2.add(booking2);
        bookings2.add(booking4);
        users.get(0).setBooking(bookings1);
        users.get(1).setBooking(bookings2);
        bookingList.add(booking1);
        bookingList.add( booking2);

    }

    @Test
    void addBooking() {



        Booking actual = module.addBooking(users,flights.get(1));
        Booking expected = bookingList.get(bookingList.size() -1);

        assertEquals(expected, actual);
    }
    @Test
    void deleteBooking() {
        module.addBooking(users,flights.get(2));
        Booking last =   bookingList.get(bookingList.size()-1);
        Booking actual = bookingList.get(bookingList.size()-2);
        module.deleteBooking(last.getId());
        Booking expected = bookingList.get(bookingList.size() -1);

        assertEquals(expected, actual);
    }



    @Test
    void findAll() {
     List <Booking> actual = module.findAll();
     List <Booking> expected = bookingList;
        assertEquals(expected, actual);

    }




}

