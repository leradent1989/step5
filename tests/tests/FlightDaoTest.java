package tests;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import step5.controller.FlightsController;
import step5.dao.FlightsDao;
import step5.service.FlightsService;
import step5.domain.Flight;
import java.time.LocalTime;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FlightDaoTest{
    private FlightsDao module;
    private ByteArrayOutputStream output = new ByteArrayOutputStream();
    List <Flight> flights = new ArrayList<>(List.of(new Flight(LocalDate.of(2021,10,13), LocalTime.of(16,40),"PARIS",5,1111),
            new Flight(LocalDate.of(2023,01,(LocalDate.now().getDayOfMonth())), LocalTime.of((LocalTime.now().getHour() +1),50),"LONDON",5,1211),
            new Flight(LocalDate.of(2021,10,13), LocalTime.of(18,30),"CHICAGO",6,1311),
            new Flight(LocalDate.of(2021,10,14), LocalTime.of(19,40),"WASHINGTON",10,1411)
    ) );

    @BeforeEach
    public void setUp() {


        module = new FlightsDao(flights);
        flights.add(new Flight(LocalDate.of(2023,2,10), LocalTime.of(19,20),"WASHINGTON",10,1711));

    }

    @Test
    void findAll() {
        List <Flight> actual =module.findAll();
        List <Flight> expected = flights;
        assertEquals(expected, actual);
    }


    @Test
    void findFlightById() {
        Flight actual =  module.findById(1211);
        Flight expected = flights.get(1);
        assertEquals(expected, actual);
    }


    @Test
    void save() {
        Flight addedFlight =  new Flight(LocalDate.of(2023,2,10), LocalTime.of(19,20),"WASHINGTON",10,1611);
        module.save(addedFlight);
        Flight actual = module.findById(addedFlight.getId());
        Flight expected = addedFlight;

        assertEquals(expected, actual);
    }


}
