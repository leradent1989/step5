package step5.service;

import step5.domain.Flight;
import step5.dao.FlightsDao;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;

public class FlightsService {
    private FlightsDao flightsDao;

    public FlightsService(FlightsDao flightsDao) {
        this.flightsDao = flightsDao;
    }

    public  void  loadData(List <Flight> flights){

        flightsDao.loadData(flights);
    }
public  List <Flight> getFlights (){

        return flightsDao.getFlights();
}
    public void addFlight(Flight flight) {
        flightsDao.save(flight);

    }

    public Flight findFlightById(int id){

      return   flightsDao.findById(id);
    }

    public Flight findFlight(LocalDate date,String destination,int passengersCount){

        List <Flight > flights =  flightsDao.findAll().stream().filter(flight -> flight.getDate().equals(date )&& flight.getDestination().equals(destination.toUpperCase(Locale.ROOT))&& flight.getFreeSeats() >= passengersCount  ).toList();
       if(flights.size() > 0){
        return flights.get(0);}
       else{
           System.out.println("Sorry,there is no such flights for this date");
           return null ;
       }
    }
    public void showAllFlightsNext24Hours(List<Flight> flights){
       flights.stream().filter(el -> LocalDateTime.of(el.getDate(),el.getTime()).isAfter(LocalDateTime.now()) && LocalDateTime.of(el.getDate(),el.getTime()).isBefore(LocalDateTime.now().plusDays(1))).forEach(flight -> System.out.println(flight.toString()));
    }

    public List<Flight> findAll() {
        return flightsDao.findAll();
    }
}
