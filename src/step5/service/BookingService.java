package step5.service;

import step5.domain.Booking;
import step5.domain.Flight;
import step5.domain.User;
import step5.dao.BookingDao;

import java.util.List;
import java.util.stream.Collectors;

public class BookingService {
    private BookingDao bookingDao;

    public BookingService(BookingDao bookingDao) {
        this.bookingDao = bookingDao;
    }
 public List <Booking> getBookings(){
        return bookingDao.findAll();
 }

    public  void  loadData(){
        bookingDao.loadData();
    }
    public  List <Booking> getBookingList (){
        return bookingDao.getBookingList();
    }


    public Booking addBooking(List <User > passengers, Flight flight) {

        return bookingDao.addBooking(passengers,flight);
    }

    public void deleteBooking(int bookingId) {
      bookingDao.deleteBooking(bookingId);
    }

    public void displayAllBookings(User passenger) {
        List <Booking> bookings = bookingDao.findAll();
    List <Booking>  userBookings = bookings.stream().filter(el -> el.getPassengerList().contains(passenger)).collect(Collectors.toList());

      userBookings.forEach(el -> System.out.println(el.toString()));

    }

}
