package step5.domain;



import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.time.LocalTime;
import java.util.List;
import java.util.Random;

public class Booking implements Serializable {
    private List<User> passengerList;
    private int passengerNum;
    private int bookingId;
    private Flight flight;

    Random random = new Random(LocalTime.now().toNanoOfDay());

    public Booking(List<User> passengerList, int passengersNum, Flight flight ) {
        this.passengerList = passengerList;
        this.passengerNum = passengersNum;
       bookingId = random.nextInt(200);
        this.flight = flight;
    }

    public List<User> getPassengerList() {
        return passengerList;
    }

    public void setPassengerList(List<User> passengerList) {
        this.passengerList = passengerList;
    }

    public int getPassengersQuantity() {
        return passengerNum;
    }

    public void setPassengersQuantity(int passengersQuantity) {
        this.passengerNum = passengersQuantity;
    }

    public int getId() {
        return bookingId;
    }

    public void setId(int idOfBooking) {
      this. bookingId = idOfBooking;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    @Override
    public String toString() {
        return "Booking ID " + bookingId +
                ", flight=" + flight +
                ", passengers: " + passengerList;
    }
    public static List <Booking> getBookings (){
        List <Booking> bookings;
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("listBookings") ) ) {
            bookings = (List<Booking>) objectInputStream.readObject();

        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return bookings;
    }
}

