package step5.domain;

public enum Destination {
    PARIS,
    AMSTERDAM,
    ANTALYA,
    LONDON,
    WASHINGTON
}
