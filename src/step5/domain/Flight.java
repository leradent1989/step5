package step5.domain;

import java.util.*;
import java.time.LocalDate;
import java.time.LocalTime;

public class Flight implements  java.io.Serializable {
    private  int id;
 private   LocalDate date;
  private  LocalTime time;
  private  String destination;
 private   int freeSeats;
 private List <User> passengers;

    public Flight(LocalDate date,LocalTime time,String destination,int freeSeats,int id ){
        this.date = date;
        this.time = time;
        this.destination = destination;
        this.freeSeats = freeSeats;
        this.id = id;
        this.passengers = new ArrayList<>() ;

    }
public  LocalDate getDate(){
        return date;
}
 public void setDate(LocalDate date){
        this.date = date;

 }
    public  LocalTime getTime(){
        return time;
    }
   public  void setTime(LocalTime time){
        this.time = time;
   }
    public String getDestination(){
        return destination;
    }
    public void setDestination(String destination){
        this.destination = destination;
    }
    public int getFreeSeats(){
        return freeSeats;
    }
   public void setFreeSeats(int freeSeats) {
        this.freeSeats = freeSeats;
   }

    public List <User> getPassengers (){
        return passengers ;
    }
    public void setPassengers (List <User> passengers ) {
        this.passengers  = passengers ;
    }
    public int getId(){
        return  id;
    }
    public  void setId(int id){
        this.id = id;
    }
   @Override

    public String toString(){
        String str = "Flight [N " + id + "; date:" + date + "; time:"+time + "; destination: " + destination + "; freeSeats:" + freeSeats + " passengers: " + passengers + " ]";

        return  str;
   }

    public static void main(String[] args) {
        Flight firstFlight = new Flight(LocalDate.of(2020,11,19),LocalTime.of(15,30),"New York",4,111);
        System.out.println(firstFlight.toString());

    }

}
