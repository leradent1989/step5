package step5.dao;

import step5.domain.Flight;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

public class FlightsDao {
    private List<Flight> flightList;

    public FlightsDao(List<Flight> flightList) {
        this.flightList = flightList;
    }

    public void loadData (List<Flight> flights){
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("listFlights"))) {
            objectOutputStream.writeObject(flights);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public List<Flight> getFlights(){
        List <Flight> flights;
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("listFlights") ) ) {
            flights = (List<Flight>) objectInputStream.readObject();
            flights.forEach(el -> el.toString());

        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return flights;
    }

    public void save(Flight flight) {
        flightList.add(flight);
    }
    public Flight findById(int id){

   List <Flight> flightById =  flightList.stream().filter(flight -> flight.getId() == id).toList();
    return flightById.get(0);

    }
    public List<Flight> findAll() {
        return flightList;
    }
}
