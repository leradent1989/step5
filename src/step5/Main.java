package step5;

import step5.controller.FlightsController;
import step5.controller.BookingController;
import step5.dao.BookingDao;

import step5.dao.FlightsDao;
import step5.domain.*;
import step5.service.BookingService;
import step5.service.FlightsService;


import java.io.*;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

public class Main {

    public static void main(String[] args) throws ParseException {



        if (  !Paths.get("listFlights").toFile().exists()) {
        FlightListCreator.generate();
        }
        List <Flight> flights  = FlightListCreator.getData();

        if (  !Paths.get("listBookings").toFile().exists()) {
            List<Booking> bookings1 = new ArrayList<>();
            List <Booking> bookings2 = new ArrayList<>();
            List <Booking> bookings3 = new ArrayList<>();

            List<User> users = new ArrayList<>(List.of(
                    new User("Valeriya", "Vitvytska", bookings1),
                    new User("Roman", "Litvinenko", bookings2),
                    new User("Olga", "Kyriliuk", bookings3)
            ));
            Booking booking1 = new Booking(users,users.size(),flights.get(54));
            Booking booking2 = new Booking(users,users.size(),flights.get(28));
            Booking booking3 = new Booking(users,users.size(),flights.get(101));
            Booking booking4 = new Booking(users,users.size(),flights.get(126));

            List<Booking> bookingList = new ArrayList<>();
            bookings1.add( booking1);
            bookings1.add(booking3);
            bookings2.add(booking2);
            bookings2.add(booking4);
            bookings3.add(booking4);
            users.get(0).setBooking(bookings1);

            users.get(1).setBooking(bookings2);
            users.get(2).setBooking(bookings3);
            bookingList.add(booking1);
            bookingList.add( booking2);
            bookingList.add( booking3);
            try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("listBookings"))) {
                objectOutputStream.writeObject(bookingList);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

        }



         FlightsDao flightsDao = new FlightsDao(flights);
        FlightsService flightsService = new FlightsService(flightsDao);
        FlightsController flightsController = new FlightsController(flightsService);

       BookingDao bookingDao = new BookingDao(Booking.getBookings());
        BookingService bookingService= new BookingService(bookingDao);
        BookingController bookingController = new BookingController(bookingService);



        List <User> users2 = new ArrayList<>() ;
        bookingController.getBookings().forEach(el -> el.getPassengerList().forEach(el2 -> users2.add(el2)));

        while (true) {
            System.out.println("Menu: ");

            System.out.println("1. Show all flights");
            System.out.println("2. Find flight by id");
            System.out.println("3. My flight reservations");
            System.out.println("4.Delete reservation ");
            System.out.println("5. Add new reservation");
            System.out.println("6.Exit");


            System.out.println("You choice: ");
            Scanner scanner = new Scanner(System.in);
            if (!scanner.hasNextInt()) {
                System.out.println("Illegal input");
                continue;
            }

            int menuItem = scanner.nextInt();

            switch (menuItem) {

                case 1:
                    try {


                 List <Flight> allFlights =  flightsController.findAll();
                 if (allFlights.size() == 0){ throw  new  Exception();}
               //  allFlights.stream().filter(el ->LocalDateTime.of(el.getDate(),el.getTime()).isAfter(LocalDateTime.now()) && LocalDateTime.of(el.getDate(),el.getTime()).isBefore(LocalDateTime.now().plusDays(1))).forEach(flight -> System.out.println(flight.toString()));
                           flightsController.showAllFlightsNext24Hours(allFlights); }
                    catch (Exception e){
                        System.err.println("LIST IS EMPTY PLEASE LOAD DATA");
                    }
                    break;


                case 2:
                    System.out.println("Enter number of flight");
                    try {

                    Scanner in =new Scanner(System.in);

                    int  id = in.nextInt();

                   Flight currentFlight = flightsController.findFlightById(id);
                   System.out.println(currentFlight.toString());}
                   catch(InputMismatchException e){
                        System.out.println("Enter number");
                   }
                    break;
                case 3:
                    System.out.println("Please enter your name");
                    Scanner scan = new Scanner(System.in);
                    String name = scan.nextLine();
                    System.out.println("Please enter your surname");
                    String surname = scan.nextLine();
                   User user  =  User.findUserInList(name,surname,users2);


                    while(user== null){
                        System.out.println("Sorry User not found Try again");
                        System.out.println("Please enter your name");
                        name = scan.nextLine();
                        System.out.println("Please enter your surname");
                        surname = scan.nextLine();
                       user = User.findUserInList(name,surname,users2);

                    }
                  bookingController.displayAllBookings(User.findUserInList(name,surname,users2));
                    break;
                case 4:
                    System.out.println("Please enter your name");
                    Scanner scan3 =new Scanner(System.in);
                    String userName = scan3.nextLine();
                    scanner.nextLine();
                    System.out.println("Please enter your surname");
                    String userSurname = scan3.nextLine();
                    System.out.println("Please enter booking number");
                try{
                    int bookingNumber = scanner.nextInt();

                 bookingController.deleteBooking(bookingNumber);
                bookingController.displayAllBookings(User.findUserInList(userName,userSurname,users2));}
                catch (Exception e){
                    System.out.println("Booking not found");
                }

                    break;

                case 5:

                    System.out.println("Please enter destination");
                    String destination = scanner.nextLine();
                    scanner.nextLine();
                    System.out.println("Please enter number of passengers");
                    int numberPassenger = scanner.nextInt();
                    scanner.nextLine();
                    System.out.println("Please enter date dd/MM/YYYY");
                    String  date = scanner.nextLine();
                    Date date2=new SimpleDateFormat("dd/MM/yyyy").parse(date);
                    long millisInADay = 24*3600*1000;

                    LocalDate date3 = LocalDate.ofEpochDay(date2.getTime()/millisInADay + 1  );


                    List <User> passengers = new ArrayList<>();
                    for(int i =0 ; i <numberPassenger; i ++) {
                        System.out.println("Please enter your name");
                        Scanner scan2 = new Scanner(System.in);
                        String name3 = scan2.nextLine();
                        System.out.println("Please enter your surname");

                        String surname3 = scan2.nextLine();
                        User user2 = User.findUserInList(name3, surname3, users2);
                     if (user2 == null) {

                            System.out.println("Please enter your name");
                            name = scan2.nextLine();
                            System.out.println("Please enter your surname");
                            surname = scan2.nextLine();
                            List <Booking> bookings = new ArrayList<>();
                            user =new User(name, surname,bookings);
                         passengers.add(User.findUserInList(name3,surname3,users2));
                        }else   passengers.add(user2);

                    }
                    System.out.println(flightsController.findFlight(date3,destination,numberPassenger));

                    System.out.println("Please enter flight number");
                    int flightId = scanner.nextInt();
                    try {


                        bookingController.addNewBooking(passengers,flightsController.findFlightById(flightId) );
                    }
                   catch (Exception e){
                        System.out.println("User not found");
                   }
                    break;
                case 6:
                    flightsController.loadData(flightsService.findAll());
                    bookingController.loadData();

                    System.exit(0);

                    break;

                default:
                    System.out.println("Illegal input");
            }
        }
    }


}
