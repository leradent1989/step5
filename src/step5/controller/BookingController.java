package step5.controller;

import step5.domain.Booking;
import step5.domain.Flight;
import step5.domain.User;
import step5.service.BookingService;

import java.util.List;

public class BookingController {

    private BookingService bookingService;

    public BookingController(BookingService bookingService) {
        this.bookingService =bookingService;

    }
    public  void  loadData(){
        bookingService.loadData();
    }
    public  List <Booking> getBookingList (){
        return bookingService.getBookingList();
    }
    public Booking addNewBooking(List<User> passengers ,Flight flight){
     return   bookingService.addBooking(passengers,flight);
    }

    public void deleteBooking(int bookingId) {
        bookingService.deleteBooking(bookingId);
    }
    public List <Booking> getBookings (){
        return bookingService .getBookings();
    }
    public void displayAllBookings(User passenger) {

        bookingService.displayAllBookings( passenger);
    }
}
